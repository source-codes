# source codes

backup directory snapshot to PDF, optimized for plain text

## features

### configurations

example configurations:

- code.mathr.co.uk

### text

- latin1 (iso-8859-1) without ±×²

- charsets that can be converted to the above

  - us-ascii
  - utf-8
  - utf-16le

- neither `listings` nor `listingsutf8` support non-ascii properly

- exceedingly long lines will break `listings` (despite line breaking)

### image

- png

- jpg / jpeg

- ppm, tif, gif, ... via imagemagick convert to png

- xcf, ico, ... via imagemagick convert to png per layer

### audio

- wav, ... via sox to spectrogram png

### filenames

- handles `~`, `_` via escape

- handles `#` via symlinks

- handles `.` multiple times in image names via symlinks

### filters

- ignores .git .hg .svn subfolders

- list of files to ignore per project

## usage

### code.mathr.co.uk

    cd source-codes
    mkdir tmp-1
    cd tmp-1
    ../code.mathr.co.uk/GET.sh
    for project in */
      ../source-codes.sh ../code.mathr.co.uk "${project}"
    done
    cd ..

### third-party

    cd source-codes
    mkdir tmp-2
    cd tmp-2
    for project in ../third-party/*/
    do
      ../third-party/GET.sh "$(basename "${project}")"
    done
    for project in */
    do
      ../source-codes.sh ../third-party "${project}"
    done
    cd ..

## requirements

- bash

- git

- pdflatex

  - geometry
  - inputenc
  - listings
  - MnSymbol
  - graphicx
  - fancyhdr

- file

- iconv

- imagemagick

- sox

## legal

source-codes -- backup directory snapshot to PDF, optimized for plain text
Copyright (C) 2019  Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
