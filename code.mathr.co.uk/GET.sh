#!/bin/bash
set -e
ME="$(dirname "$(readlink -e "${0}")")"
HOST="https://code.mathr.co.uk"
wget -O - "${HOST}/?a=project_index" |
sort |
cut -d\  -f 1 |
while read PROJECT
do
  if ! grep -q -F "${PROJECT}" "${ME}/EXCLUDE"
  then
    if [ -d "${PROJECT}" ]
    then
      pushd "${PROJECT}"
      git fetch origin
      git merge origin/HEAD
      popd
    else
      git clone "${HOST}/${PROJECT}.git"
      pushd "${PROJECT}"
      if [ -x "${ME}/${PROJECT}/PREPARE.sh" ]
      then
        "${ME}/${PROJECT}/PREPARE.sh"
      fi
      popd
    fi
  fi
done
