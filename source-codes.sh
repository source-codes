#!/bin/bash
set -e
ME="$(dirname "$(readlink -e "${0}")")"
config="${1}"
project="${2%/}"
code="$(pwd)"
if [ ! -d "${config}" ]
then
  printf "%s\n" "configuration directory \"${config}\" not found"
  exit 1
fi
if [ ! -d "${project}" ]
then
  printf "%s\n" "configuration directory \"${project}\" not found"
  exit 1
fi
if [ "${project}.pdf" -nt "${project}" ]
then
  printf "%s\n" "${project}.pdf is newer than ${project}, skipping"
  exit
fi
get_year0() {
  (
    pushd "${code}/${project}" > /dev/null
    head -n 1 "${code}/${config}/${project}/YEAR" 2>/dev/null
    head -n 1 "${code}/${config}/YEAR" 2>/dev/null
    ( [ -d .git ] && git log --pretty=format:%ai --reverse ) ||
    ( [ -d .hg ] && hg log -r "branch(default) and 0:" -l 1 --template "{date|isodate}\n" ) ||
    ( [ -d .svn ] && svn log -l 1 -r 1 | cut -d\|  -f 3 | cut -d\  -f 2 | cut -d- -f 1 | head -n +2 | tail -n 1 ) ||
    echo "????"
    popd > /dev/null
  ) | head -c 4
}
get_year1() {
  (
    pushd "${code}/${project}" > /dev/null
    tail -n +2 "${code}/${config}/${project}/YEAR" 2>/dev/null
    tail -n +2 "${code}/${config}/YEAR" 2>/dev/null
    ( [ -d .git ] && git log --pretty=format:%ai -n 1 ) ||
    ( [ -d .hg ] && hg log -l 1 --template "{date|isodate}\n" ) ||
    ( [ -d .svn ] && svn log -l 1 | cut -d\| -f 3 | cut -d\  -f 2 | cut -d- -f 1 | head -n +2 | tail -n 1 ) ||
    echo "????"
    popd > /dev/null
  ) | head -c 4
}
authors="$(cat "${config}/${project}/AUTHORS" 2>/dev/null || cat "${config}/AUTHORS" 2>/dev/null || echo "????")"
year0="$(get_year0)"
year1="$(get_year1)"
if [ "${year0}" = "${year1}" ]
then
  years="${year0}"
else
  years="${year0}--${year1}"
fi
(
  echo .git
  echo .hg
  echo .svn
  if [ -f "${config}/${project}/EXCLUDE" ]
  then
    cat "${config}/${project}/EXCLUDE"
  fi
) |
while read pattern
do
  for thing in $(compgen -G "${project}/${pattern}")
  do
    find "${thing}"
  done
done > "${project}.EXCLUDE"
rm -rf "${project}.REPLACE" "${project}.aux" "${project}.log" "${project}.toc"
(
  cat <<EOF
\documentclass[twoside,10pt]{extreport}
\usepackage[paperwidth=210mm,paperheight=297mm,margin=25mm,includehead,includefoot]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{MnSymbol}
\usepackage{graphicx}

\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{#1}}
\fancyhf{}
\fancyhf[HRE,HLO]{${project}}
\fancyhf[HLE,HRO]{\rightmark}
\fancyhf[FLE,FRO]{\thepage}

\renewcommand{\thesection}{\arabic{section}}

\title{${project}}
\date{${years}}
\author{${authors}}

\lstset{
  inputencoding=latin1,
  extendedchars=true,
  basicstyle=\small,
  numbers=left,
  numberstyle=\footnotesize,
  firstnumber=1,
  stepnumber=5,
  numbersep=15pt,
  showspaces=false,
  showstringspaces=false,
  showtabs=false,
  tabsize=8,
  breaklines=true,
  breakatwhitespace=false,
  prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rhookswarrow}},
  postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rcurvearrowse\space}}
}

\begin{document}
\maketitle
\tableofcontents
EOF
  find "${project}/" -type f -print |
  sort |
  while read file
  do
    if ! grep -q -F "${file}" "${project}.EXCLUDE"
    then
      filetype="$(file -ib "${file}")"
      mimetype="$(printf "%s" "${filetype}" | cut -d\  -f 1 | cut -d\; -f 1)"
      charset="$(printf "%s" "${filetype}" | cut -d\  -f 2 | sed 's/^charset=//g')"
      mimeclass="$(printf "%s" "${filetype}" | cut -d/ -f 1)"
      title="$(printf "%s" "${file#${project}/}" | sed 's/~/\\char`\\~/g' | sed 's/_/\\_/g' | sed 's/#/\\#/g')"
      filenohash="$(printf "%s" "${file}" | sed 's/#/_/g')"
      if [ "${charset}" = us-ascii ]
      then
        if [ "${filenohash}" != "${file}" ]
        then
          mkdir -p "${project}.REPLACE/$(dirname "${file}")"
          filenohash="${project}.REPLACE/${filenohash}"
          ln -s -f "${code}/${file}" "${filenohash}"
          file="${filenohash}"
        fi
        file="$(printf "%s" "${file}" | sed 's/~/\\string~/g')"
        echo "\\section{${title}} \\lstinputlisting{${file}}"
      elif [ "${charset}" = iso-8859-1 -o "${charset}" = utf-16le ]
      then
        mkdir -p "${project}.REPLACE/$(dirname "${file}")"
        filelatin1="${project}.REPLACE/${filenohash}"
        if iconv -f "${charset}" -t utf-8 < "${file}" | sed "s/±/+-/g" | sed "s/×/*/g" | sed "s/²/^2/g" | iconv -f utf-8 -t latin1//TRANSLIT > "${filelatin1}"
        then
          file="$(printf "%s" "${filelatin1}" | sed 's/~/\\string~/g')"
          echo "\\section{${title}} \\lstinputlisting{${file}}"
        else
          echo "\\section{${title}} (ERROR: could not convert text to latin1 from \"${filetype}\" via utf-8)"
          echo "ERROR: could not convert text file \"${file}\" to latin1 from \"${filetype}\" via utf-8" >&2
        fi
      elif [ "${charset}" = utf-8 ]
      then
        mkdir -p "${project}.REPLACE/$(dirname "${file}")"
        filelatin1="${project}.REPLACE/${filenohash}"
        if sed "s/±/+-/g" < "${file}" | sed "s/×/*/g" | sed "s/²/^2/g" | iconv -f utf-8 -t latin1//TRANSLIT > "${filelatin1}"
        then
          file="$(printf "%s" "${filelatin1}" | sed 's/~/\\string~/g')"
          echo "\\section{${title}} \\lstinputlisting{${file}}"
        else
          echo "\\section{${title}} (ERROR: could not convert text to latin1 from \"${filetype}\")"
          echo "ERROR: could not convert text file \"${file}\" to latin1 from \"${filetype}\"" >&2
        fi
      elif [ "${mimeclass}" = image ]
      then
        # would like to check ${mimetype} for image/png or image/jpeg
        # but that is insufficient
        # check longest file extension: if there is more than one dot
        # in the filename, \includegraphics{} breaks with unknown type
        ext="$(printf "%s" "$(basename ${file})" | sed "s/^[-.]*[.]//g")"
        if [ "${ext}" = png -o "${ext}" = jpg -o "${ext}" = jpeg ]
        then
          file="$(printf "%s" "${file}" | sed 's/~/\\string~/g')"
          echo "\\section{${title}} \\includegraphics[width=0.5\\textwidth,height=0.5\\textwidth,keepaspectratio=true]{${file}}"
        else
          # some other image, convert to PNG
          mkdir -p "${project}.REPLACE/$(dirname "${file}")"
          filepng="${project}.REPLACE/$(dirname "${file}")/$(basename "${filenohash}" | sed 's/[.]/_/g')"
          convert "${file}" "${filepng}.png"
          if [ -f "${filepng}.png" ]
          then
            # a single file resulted
            file="$(printf "%s" "${filepng}.png" | sed 's/~/\\string~/g')"
            echo "\\section{${title}} \\includegraphics[width=0.5\\textwidth,height=0.5\\textwidth,keepaspectratio=true]{${file}}"
          else
            # multiple files from layers
            echo "\\section{${title}}"
            count="$(compgen -G "${filepng}-*.png" | wc -l)"
            for n in $(seq 0 $(( count - 1 )))
            do
              convert "${filepng}-${n}.png" -set page +0+0 -background black -flatten "${filepng}-${n}-alpha.png"
              file="$(printf "%s" "${filepng}" | sed 's/~/\\string~/g')"
              echo "\\subsection{Layer ${n}} \\includegraphics[width=0.5\\textwidth,height=0.5\\textwidth,keepaspectratio=true]{${file}-${n}-alpha.png}"
            done
          fi
        fi
      elif [ "${mimeclass}" = audio ]
      then
        # replace dots in audio filename to avoid \includegraphics break
        # on longest possible extension not being a recognized image type
        spectrogram="${project}.REPLACE/$(dirname "${file}")/$(basename "${file}" | sed 's/[.]/_/g').png"
        mkdir -p "$(dirname "${spectrogram}")"
        sox "${file}" -n spectrogram -x 1920 -y 540 -m -l -o "${spectrogram}"
        file="$(printf "%s" "${spectrogram}" | sed 's/~/\\string~/g')"
        echo "\\section{${title}} \\includegraphics[width=0.5\\textwidth,height=0.5\\textwidth,keepaspectratio=true]{${file}}"
      elif [ "${mimetype}" = "inode/x-empty" ]
      then
        echo "\\section{${title}} (empty file)"
      else
        echo "\\section{${title}} (ERROR: don't know how to handle \"${filetype}\")"
        echo "ERROR: don't know how to handle file \"${file}\" (\"${filetype}\")" >&2
      fi
    fi
  done
  cat <<EOF
\end{document}
EOF
) > "${project}.tex" 2> "${project}.ERROR"
pdflatex "${project}.tex"
pdflatex "${project}.tex"
pdflatex "${project}.tex"
if [ ! -s "${project}.ERROR" ]
then
  rm -rf "${project}.REPLACE" "${project}.aux" "${project}.log" "${project}.toc" "${project}.EXCLUDE" "${project}.ERROR" "${project}.tex"
fi
