#!/bin/bash
set -e
ME="$(dirname "$(readlink -e "${0}")")"
project="${1}"
if [ ! -d "${ME}/${project}" ]
then
  echo "ERROR: unknown project \"${project}\""
  exit 1
fi
if [ -d "${project}" ]
then
  # project already exists, update
  pushd "${project}"
  [ -d .git ] && git pull
  [ -d .hg ] && hg pull
  [ -d .svn ] && svn update
  popd
else
  # project not there, clone
  [ -f "${ME}/${project}/GIT" ] && git clone "$(cat "${ME}/${project}/GIT")"
  [ -f "${ME}/${project}/HG" ] && hg clone "$(cat "${ME}/${project}/HG")"
  [ -f "${ME}/${project}/SVN" ] && svn checkout "$(cat "${ME}/${project}/SVN")" "${project}"
  # run post-clone script
  [ -x "${ME}/${project}/PREPARE.sh" ] && "${ME}/${project}/PREPARE.sh"
fi
