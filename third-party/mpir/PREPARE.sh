#!/bin/bash
sed -i "s/$(printf "\\x92")/'/g" mpir.net/mpir.net/HugeInt.h
for f in tests/mpz/t-tdiv.c
do
  fold -s < "${f}" > "${f}.folded" &&
  mv "${f}".folded "${f}"
done
